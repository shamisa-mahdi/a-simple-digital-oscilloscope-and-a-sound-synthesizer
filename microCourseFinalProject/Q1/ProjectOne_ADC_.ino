const int analogPin = A0;
unsigned int samplingRate = 0;
unsigned int sampleNum = 0;
unsigned char myArr[1200]; //genrating an array to store samples

byte checksum(byte arr[], int l);
void testTheProgram();
void testMaximumSampleRate();

void setup() {
  pinMode(analogPin, INPUT);         // configure analogPin
  analogReference(DEFAULT); //(DEFAULT, INTERNAL, INTERNAL1V1, INTERNAL2V56, EXTERNAL)
  Serial.begin(115200);              //  setup serial

  //testTheProgram();
  //testMaximumSampleRate();
}

void loop() {
  /* Computer and UC are ready */
  while(!Serial.available()) {}
  if('S' == Serial.read()) {
    Serial.print('S');

    /* reading samplingRate */
    while(!Serial.available()) {}
    samplingRate = Serial.read() << 8;
    Serial.print('M');
    
    while(!Serial.available()) {}
    samplingRate += Serial.read();
    Serial.print('L');
    if(samplingRate <= 8700) {
      /* reading the number of samples */
      while(!Serial.available()) {}
      sampleNum = Serial.read() << 8;
      Serial.print('A');
      
      while(!Serial.available()) {}
      sampleNum += Serial.read();
      Serial.print('B');          //sampling finished

      long P = (1. / samplingRate * 1E6 - 113)/2;
      
      //long t0 = millis();
      for(unsigned int i = 0; i < sampleNum; i++) {
        myArr[i] = analogRead(analogPin)>>2;
        delayMicroseconds(P);
        delayMicroseconds(P);
      }    
//      long deltaT = millis() - t0;      
//      Serial.println("Finished");
//      Serial.print("Time Of Each ADC: ");
//      Serial.println((deltaT*1000)/sampleNum);
     
//      for(unsigned int i = 0; i < sampleNum; i++) {
//        Serial.print(i);
//        Serial.print(": ");
//        Serial.print(myArr[i]);
//        Serial.print(", ");
//      }
      
      unsigned int i = 0;
      while(i < sampleNum) {
        while(!Serial.available()) {}
        if('N' == Serial.read()) {
          Serial.write(myArr[i]);
          i++;
        }
      }

      while(1) {
        while(!Serial.available()) {}
        if('E' == Serial.read()) {
          Serial.write(checksum(myArr, sampleNum));
          break;
        }
      }
    } else
      Serial.print("wrong sampling rate!");
  }
}

void testTheProgram() {
  samplingRate = 500;
  sampleNum = 500;
  long P = (1. / samplingRate * 1E6 - 113)/2;
  Serial.print("P: ");
  Serial.println(P);
  
  Serial.print("Start...");
  
  long t0 = millis();
  for(unsigned int i = 0; i < sampleNum; i++) {
    myArr[i] = analogRead(analogPin)>>2;
    delayMicroseconds(P);
    delayMicroseconds(P);
  }    
  long deltaT = millis() - t0;
  
  Serial.println("Finished");
  Serial.print("Time Of Each ADC: ");
  Serial.println((deltaT*1000)/sampleNum);
 
  for(int i = 0; i < sampleNum; i++) {
    Serial.print(i);
    Serial.print(": ");
    Serial.print(myArr[i]);
    Serial.print(", ");
  }
}

void testMaximumSampleRate() {
    unsigned int num = 1000; 

    unsigned long t0 = micros();

    for(unsigned int i = 0; i < num; i++) {
      myArr[i] = analogRead(analogPin) >> 2;
    }

    unsigned long deltaT = micros() - t0;
    Serial.print("Time Of Each ADC: ");
    Serial.println(deltaT/num);

}

byte checksum(byte arr[], int l) {
  byte val = 0;
  for(int i = 0; i < l; i++) {
    val += arr[i];
  }
  return val;
}

