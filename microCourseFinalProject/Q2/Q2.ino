#include "noteFrequencies.h"

const unsigned char notePin = 8;

float getFrequency(const unsigned int& noteNumber);
void myTone(const unsigned int& notePin, const float& noteFrequency,
                              const unsigned int& noteDuration);
void testTheProgram();

void setup() {
  Serial.begin(115200);
  pinMode(notePin, OUTPUT);
  //testTheProgram();
}

void loop() {
  while(!Serial.available()) {}
  
  if(Serial.read() == 'S') {
    Serial.print('S');
    
    while(!Serial.available()) {}
    unsigned int noteNumber = Serial.read();
    Serial.print('N');
    //Serial.print(noteNumber);
    
    while(!Serial.available()) {}
    unsigned int noteDuration = Serial.read() * 50;
    Serial.print('D');
    //Serial.print(noteDuration);
    float noteFrequency = getFrequency(noteNumber);
    myTone(notePin, noteFrequency, noteDuration);
  }
  //testTheProgram();
}

float getFrequency(const unsigned int& noteNumber) {
  switch(noteNumber) {
    case 0: return _F0;
    case 1: return _F1;
    case 2: return _F2;
    case 3: return _F3;
    case 4: return _F4;
    case 5: return _F5;
    case 6: return _F6;
    case 7: return _F7;
    case 8: return _F8;
    case 9: return _F9;
    case 10: return _F10;
    case 11: return _F11;
    case 12: return _F12;
    case 13: return _F13;
    case 14: return _F14;
    case 15: return _F15;
    case 16: return _F16;
    case 17: return _F17;
    case 18: return _F18;
    case 19: return _F19;
    case 20: return _F20;
    case 21: return _F21;
    case 22: return _F22;
    case 23: return _F23;
    
    default: return 1;
  }
}

void myTone(const unsigned int& notePin, const float& noteFrequency,
                              const unsigned int& noteDuration) {
  float T = 1000000.1/(float)noteFrequency ;
  long int p = T / 2;
  float N = noteDuration / (T/1000);
  for(unsigned int i = 0; i < N; i++) {
    digitalWrite(notePin, HIGH);
    delayMicroseconds(p);
    digitalWrite(notePin, LOW);
    delayMicroseconds(p);
  }
  
  delay(100);
}

void testTheProgram() {
  unsigned int noteDuration0 = 2000;
  
  for(unsigned int i = 0; i < 24; i++) {
    unsigned int noteFrequency = getFrequency(i);
    Serial.print("Note ");
    Serial.println(i); 
    if(noteFrequency >= 31) {
      tone(notePin, noteFrequency, noteDuration0);
      delay(noteDuration0 + 100);
    }
    
    else {
      myTone(notePin, noteFrequency, noteDuration0);
    }
  }

  Serial.println("END OF TEST");
}

