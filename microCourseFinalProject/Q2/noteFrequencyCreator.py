f0 = 20000
for n in range(24):
    fn = (2 ** (float(n-49)/12)) * f0
    print("#define _F{} {:.0f}".format(n, fn))